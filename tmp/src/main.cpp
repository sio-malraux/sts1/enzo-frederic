#include <libpq-fe.h>
#include <iostream>
#include <iomanip>

enum CODES_ERREUR
{
  OK_TOUT_VA_BIEN,
  ERR_NO_PING,
  ERR_NO_CONNECTION
};

int main()
{
  int code_retour;
  char info_connexion[] = "host=postgresql.bts-malraux72.net port=5432 dbname=e.casciano user=e.casciano password=P@ssword";
  PGPing code_retour_ping;
  PGconn *connexion;
  code_retour = OK_TOUT_VA_BIEN;
  code_retour_ping = PQping(info_connexion);
  int nb_caracteres;

  if(code_retour_ping == PQPING_OK)
  {
    std::cout << "Le serveur est accessible" << std::endl;
    connexion = PQconnectdb(info_connexion);

    if(PQstatus(connexion) == CONNECTION_OK)//établissement de la connexion ou affichage des erreurs
    {
      std::cout << "La connexion au serveur " << PQhost(connexion)  << "' a été établie avec les paramètres suivants :" << std::endl;
      std::cout << " * utilisateur : " << PQuser(connexion) << std::endl;
      std::cout << " * mot de passe : ";
      nb_caracteres = sizeof PQpass(connexion);

      for(int i = 0; i < nb_caracteres; ++i)
      {
        std::cout << '*';
      }

      std::cout << " (<-- autant de '*' que la longueur du mot de passe)" << std::endl;
      std::cout << " * base de données : " << PQdb(connexion) << std::endl;
      std::cout << " * port TCP : " << PQport(connexion) << std::endl;
      std::cout << std::boolalpha << " * chiffrement SSL : " << (bool)(PQgetssl(connexion) != 0) << std::endl;
      std::cout << " * encodage : " << pg_encoding_to_char(PQclientEncoding(connexion)) << std::endl;
      std::cout << " * version du protocole : " << PQprotocolVersion(connexion) << std::endl;
      std::cout << " * version du serveur : " << PQserverVersion(connexion) << std::endl;
      std::cout << " * version de la bibliothèque 'libpq' du client : " << PQlibVersion() << std::endl;
      // Récupération des données
      const char requete_sql[] = "SET schema 'si6';  SELECT * FROM \"Animal\" INNER JOIN \"Race\" ON \"Animal\".race_id = \"Race\".id WHERE sexe = 'Femelle' AND \"Race\".nom = 'Singapura';";
      PGresult *resultat = PQexec(connexion, requete_sql);
      ExecStatusType etat_execution = PQresultStatus(resultat);

      // Vérification de l'état des données reçues
      if(etat_execution == PGRES_TUPLES_OK)
      {
        int nbColonnes = PQnfields(resultat);
        int nbEnregistrements = PQntuples(resultat);
        int trait = 0;

        for(int j = 0; j < PQntuples(resultat); j++)
        {
          for(int k = 0; k < nbColonnes; k++)
          {
            if(PQgetlength(resultat, j, k) > trait)
            {
              trait = PQgetlength(resultat, j, k);
            }
          }
        }

        for(int i = 0; i < nbColonnes; i++)
        {
          std::cout << "|" << std::setw(trait) << std::left << PQfname(resultat, i);
        }

        for(int j = 0; j < PQntuples(resultat); j++)
        {
          for(int k = 0; k < nbColonnes; k++)
          {
            std::cout << "|" << std::setw(trait) << std::left <<  PQgetvalue(resultat, j, k);
          }

          std::cout << std::endl;
        }

        std::cout << "\nL'exécution de la requête SQL a retourné " << nbEnregistrements << " enregistrements." << std::endl;
        PQclear(resultat);
      }
      else if(etat_execution == PGRES_EMPTY_QUERY)
      {
        std::cerr << "La chaîne envoyée était vide." << std::endl;
      }
      else if(etat_execution == PGRES_COMMAND_OK)
      {
        std::cerr << "La requête SQL n'a renvoyé aucune donnée." << std::endl;
      }
      else if(etat_execution == PGRES_BAD_RESPONSE)
      {
        std::cerr << "La réponse du serveur n'a pas été comprise." << std::endl;
      }
      else if(etat_execution == PGRES_NONFATAL_ERROR)
      {
        std::cerr << "Une erreur non fatale (une note ou un avertissement) est survenue." << std::endl;
      }
      else if(etat_execution == PGRES_FATAL_ERROR)
      {
        std::cerr << "Une erreur fatale est survenue." << std::endl;
      }
      else
      {
        std::cerr << "Erreur de connexion" << std::endl;
      }
    }
    else
    {
      std::cerr << "Le serveur n'est pas accessible pour le moment" << std::endl;
      code_retour = ERR_NO_PING;
    }

    return code_retour;
  }
}
